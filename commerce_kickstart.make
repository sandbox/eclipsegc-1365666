api = 2
core = 7.x

; Dependencies =================================================================

projects[addressfield][type] = module
projects[addressfield][subdir] = contrib
projects[addressfield][version] = 1.0-beta2

projects[ctools][type] = module
projects[ctools][subdir] = contrib
projects[ctools][version] = 1.x-dev
;projects[ctools][download][type] = git
;projects[ctools][download][url] = http://git.drupal.org/project/ctools.git
;projects[ctools][download][branch] = 7.x-1.x
projects[ctools][patch][] = http://drupal.org/files/entity-field-value_0.patch
projects[ctools][patch][] = http://drupal.org/files/page-manager-language-support.patch
projects[ctools][patch][] = http://drupal.org/files/1275886-ctools-block-integration.patch
projects[ctools][patch][] = http://drupal.org/files/entity_field_view_modes.patch
projects[ctools][patch][] = http://drupal.org/files/language.diff
projects[ctools][patch][] = http://drupal.org/files/page-manager-admin-paths_3.patch
projects[ctools][patch][] = http://drupal.org/files/entity-child-plugin.patch
projects[ctools][patch][] = http://drupal.org/files/empty-context.patch

projects[entity][type] = module
projects[entity][subdir] = contrib
projects[entity][version] = 1.0-rc1

projects[rules][type] = module
projects[rules][subdir] = contrib
projects[rules][version] = 2.0

projects[views][type] = module
projects[views][subdir] = contrib
projects[views][version] = 3.x-dev

; Drupal Commerce and Commerce contribs ========================================

projects[commerce][type] = module
projects[commerce][subdir] = contrib
projects[commerce][version] = 1.1
;projects[commerce][download][type] = git
;projects[commerce][download][url] = http://git.drupal.org/project/commerce.git
;projects[commerce][download][branch] = 7.x-1.x
projects[commerce][patch][] = http://drupal.org/files/product_field_from_entity_0.patch

projects[commerce_authnet][type] = module
projects[commerce_authnet][subdir] = contrib
projects[commerce_authnet][version] = 1.x-dev

projects[commerce_paypal][type] = module
projects[commerce_paypal][subdir] = contrib
projects[commerce_paypal][version] = 1.x-dev

; Drupal Non-commerce Contrib Modules =======================================

projects[features][subdir] = contrib
projects[features][version] = 1.x-dev

projects[libraries][subdir] = contrib
projects[libraries][version] = 1.x-dev

projects[facetapi][subdir] = contrib
projects[facetapi][type] = module
projects[facetapi][version] = 1.0-beta8

projects[search_api][subdir] = contrib
projects[search_api][type] = module
projects[search_api][version] = 1.0-beta10

projects[search_api_solr][subdir] = contrib
projects[search_api_solr][type] = module
projects[search_api_solr][version] = 1.0-beta3

libraries[SolrPhpClient][download][type] = "get"
libraries[SolrPhpClient][download][url] = "http://solr-php-client.googlecode.com/files/SolrPhpClient.r22.2009-11-09.tgz"
libraries[SolrPhpClient][destination] = "modules/contrib/search_api_solr"

projects[panels][subdir] = contrib
projects[panels][type] = module
projects[panels][version] = 3.x-dev

